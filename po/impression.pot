# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the impression package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: impression\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-09-20 12:55-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/io.gitlab.adhami3310.Impression.desktop.in.in:3
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:5
#: data/resources/blueprints/window.blp:9
#: data/resources/blueprints/window.blp:21
#: data/resources/blueprints/window.blp:146 src/main.rs:44 src/window.rs:435
msgid "Impression"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.desktop.in.in:4
msgid "Media Writer"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.desktop.in.in:5
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:11
msgid "Create bootable drives"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.desktop.in.in:11
msgid "usb;flash;writer;bootable;drive;iso;img;disk;image"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.gschema.xml.in:6
msgid "Window width"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.gschema.xml.in:10
msgid "Window height"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.gschema.xml.in:14
msgid "Window maximized state"
msgstr ""

#. developer_name tag deprecated with Appstream 1.0
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:7
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:9
msgid "Khaleel Al-Adhami"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:17
msgid ""
"Write disk images to your drives with ease. Select an image, insert your "
"drive, and you're good to go! Impression is a useful tool for both avid "
"distro-hoppers and casual computer users."
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:40
msgid "Screen with a choice of a local image or internet download"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:44
msgid "Screen with a chosen ISO and available USB memories"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:48
msgid "Writing the ISO in progress"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:52
msgid "Success screen with a big check mark"
msgstr ""

#: data/resources/blueprints/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: data/resources/blueprints/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Open File"
msgstr ""

#: data/resources/blueprints/help-overlay.blp:19
msgctxt "shortcut window"
msgid "New Window"
msgstr ""

#: data/resources/blueprints/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: data/resources/blueprints/help-overlay.blp:29
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: data/resources/blueprints/window.blp:25
msgid "Drop Here to Open"
msgstr ""

#: data/resources/blueprints/window.blp:35
#: data/resources/blueprints/window.blp:156
#: data/resources/blueprints/window.blp:254
msgid "Main Menu"
msgstr ""

#: data/resources/blueprints/window.blp:54
msgid "Choose Image"
msgstr ""

#: data/resources/blueprints/window.blp:64
msgid "Open File…"
msgstr ""

#: data/resources/blueprints/window.blp:74
msgid "Direct Download"
msgstr ""

#: data/resources/blueprints/window.blp:108
msgid "No Connection"
msgstr ""

#: data/resources/blueprints/window.blp:109
msgid "Connect to the internet to view available images"
msgstr ""

#: data/resources/blueprints/window.blp:218
msgid "All data on the selected drive will be erased"
msgstr ""

#: data/resources/blueprints/window.blp:221
msgid "Write"
msgstr ""

#: data/resources/blueprints/window.blp:283
msgid "No Drives"
msgstr ""

#: data/resources/blueprints/window.blp:284
msgid "Insert a drive to write to"
msgstr ""

#: data/resources/blueprints/window.blp:295
msgid "Writing Completed"
msgstr ""

#: data/resources/blueprints/window.blp:296
msgid "The drive can be safely removed"
msgstr ""

#: data/resources/blueprints/window.blp:305
msgid "Finish"
msgstr ""

#: data/resources/blueprints/window.blp:326
msgid "Writing Unsuccessful"
msgstr ""

#: data/resources/blueprints/window.blp:335
msgid "Retry"
msgstr ""

#: data/resources/blueprints/window.blp:355 src/window.rs:344 src/window.rs:373
msgid "Writing"
msgstr ""

#: data/resources/blueprints/window.blp:357 src/window.rs:343
msgid "Do not remove the drive"
msgstr ""

#: data/resources/blueprints/window.blp:372 src/window.rs:258 src/window.rs:295
msgid "_Cancel"
msgstr ""

#: data/resources/blueprints/window.blp:400
msgid "Keyboard Shortcuts"
msgstr ""

#: data/resources/blueprints/window.blp:405
msgid "About Impression"
msgstr ""

#: src/window.rs:254
msgid "Stop Writing?"
msgstr ""

#: src/window.rs:255
msgid "This might leave the drive in a faulty state"
msgstr ""

#: src/window.rs:258
msgid "_Stop"
msgstr ""

#: src/window.rs:288
msgid "Erase Drive?"
msgstr ""

#: src/window.rs:289
msgid "You will lose all data stored on {}"
msgstr ""

#: src/window.rs:296
msgid "_Erase"
msgstr ""

#: src/window.rs:337 src/window.rs:367
msgid "Writing will begin once the download is completed"
msgstr ""

#: src/window.rs:339 src/window.rs:372
msgid "Downloading Image"
msgstr ""

#: src/window.rs:369
msgid "This could take a while"
msgstr ""

#: src/window.rs:397
msgid "Failed to write image"
msgstr ""

#: src/window.rs:409
msgid "Image Written"
msgstr ""

#: src/window.rs:674
msgid "Disk Images"
msgstr ""

#: src/window.rs:700
msgid "File is not a Disk Image"
msgstr ""

#: src/window.rs:804
msgid "translator-credits"
msgstr ""

#: src/window.rs:806
msgid "Code borrowed from"
msgstr ""

#: src/flash.rs:95
msgid "Failed to open disk"
msgstr ""

#: src/flash.rs:145
msgid "Failed to extract drive"
msgstr ""

#: src/flash.rs:203
msgid "Failed to download image"
msgstr ""

#: src/flash.rs:256
msgid "Writing to disk failed"
msgstr ""
